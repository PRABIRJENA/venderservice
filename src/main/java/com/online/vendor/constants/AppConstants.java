package com.online.vendor.constants;

public class AppConstants {

	private AppConstants() {
		super();
	}

	public static final String VENDOR_NAME_NOT_FOUND = "Vendor name not found";
	public static final int VENDOR_NAME_STATUS_CODE = 4000;
	public static final String ITEM_NAME_NOT_FOUND = "Item name not found";
	public static final int ITEM_NAME_STATUS_CODE = 4001;

}
