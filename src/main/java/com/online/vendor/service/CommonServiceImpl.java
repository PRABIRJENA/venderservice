package com.online.vendor.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.vendor.constants.AppConstants;
import com.online.vendor.dto.ItemResponseDTO;
import com.online.vendor.dto.VendorResponseDTO;
import com.online.vendor.entity.Item;
import com.online.vendor.entity.Vendor;
import com.online.vendor.exception.ExceptionResponse;
import com.online.vendor.exception.ItemNameNotFoundException;
import com.online.vendor.exception.VendorNameNotFoundException;
import com.online.vendor.repository.ItemRepository;
import com.online.vendor.repository.VendorRepository;

/**
 * @author prabirkumar.jena
 *
 */
@Service
public class CommonServiceImpl {

	@Autowired
	VendorRepository vendorRepository;

	@Autowired
	ItemRepository itemRepository;

	/**
	 * 
	 * @param vendorName
	 * @return List<ItemResponseDTO>
	 * @exception VendorNameNotFoundException
	 */
	public List<ItemResponseDTO> getVendorsByVendorName(String vendorName) {

		Vendor vendor = vendorRepository.findByVendorName(vendorName);
		ItemResponseDTO itemResponseDTO = null;
		if (vendor != null) {
			List<ItemResponseDTO> itemResponseDTOs = new ArrayList<>();
			for (Item item : vendor.getItems()) {
				itemResponseDTO = new ItemResponseDTO();
				itemResponseDTO.setItemId(item.getItemId());
				itemResponseDTO.setItemName(item.getItemName());
				itemResponseDTO.setPrice(item.getItemPrice());
				itemResponseDTOs.add(itemResponseDTO);
			}
			return itemResponseDTOs;
		} else {
			ExceptionResponse exceptionResponse = new ExceptionResponse();
			exceptionResponse.setStatus(AppConstants.VENDOR_NAME_STATUS_CODE);
			exceptionResponse.setMessage(AppConstants.VENDOR_NAME_NOT_FOUND);
			throw new VendorNameNotFoundException("Vendor name not found");
		}

	}

	/**
	 * 
	 * @param itemName
	 * @return List<VendorResponseDTO>
	 * @exception ItemNameNotFoundException
	 */
	public List<VendorResponseDTO> getItemsByItemName(String itemName) {

		Item item = itemRepository.findByItemName(itemName);
		VendorResponseDTO vendorResponseDTO = null;
		if (item != null) {
			List<VendorResponseDTO> vendorResponseDTOs = new ArrayList<>();
			for (Vendor vendor : item.getVendors()) {
				vendorResponseDTO = new VendorResponseDTO();
				vendorResponseDTO.setItemId(item.getItemId());
				vendorResponseDTO.setItemName(item.getItemName());
				vendorResponseDTO.setPrice(item.getItemPrice());
				vendorResponseDTO.setVendorId(vendor.getVendorId());
				vendorResponseDTO.setVendorName(vendor.getVendorName());
				vendorResponseDTOs.add(vendorResponseDTO);
			}

			return vendorResponseDTOs;
		} else {
			ExceptionResponse exceptionResponse = new ExceptionResponse();
			exceptionResponse.setStatus(AppConstants.ITEM_NAME_STATUS_CODE);
			exceptionResponse.setMessage(AppConstants.ITEM_NAME_NOT_FOUND);
			throw new ItemNameNotFoundException("Item name not found");
		}
	}
}
