package com.online.vendor.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.online.vendor.entity.Item;
import com.online.vendor.exception.ItemNotFoundException;
import com.online.vendor.repository.ItemRepository;

/**
 * 
 * @author janbee
 *
 */
@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	ItemRepository itemRepository;

	@Override
	public boolean checkAvailabilityOfItems(List<Integer> itemIdList) {
		List<Item> item = itemRepository.findAllById(itemIdList);
		return !item.isEmpty();

	}
	public Item getImtemDetailsByItemId(Integer itemId) {
		Optional<Item> item = itemRepository.findById(itemId);
		if (item.isPresent()) {
			return item.get();
		}
		throw new ItemNotFoundException("Item not Found");
	}
}
