package com.online.vendor.exception;

/**
 * @author prabirkumar.jena
 *
 */
public class ExceptionResponse {

	private String message;
	private int status;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * 
	 * @param message
	 * @param status
	 */
	public ExceptionResponse(String message, int status) {
		super();
		this.message = message;
		this.status = status;
	}
	public ExceptionResponse() {
		super();
	}
	
}
