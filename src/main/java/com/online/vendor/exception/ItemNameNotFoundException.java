package com.online.vendor.exception;

/**
 * @author prabirkumar.jena
 *
 */
public class ItemNameNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ItemNameNotFoundException() {
		super();
	}

	public ItemNameNotFoundException(String message) {
		super(message);
	}

}
