package com.online.vendor.exception;

/**
 * @author prabirkumar.jena
 *
 */
public class VendorNameNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public VendorNameNotFoundException() {
		super();
	}

	public VendorNameNotFoundException(String message) {
		super(message);
	}
}
