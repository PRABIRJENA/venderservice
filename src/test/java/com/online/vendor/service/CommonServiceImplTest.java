package com.online.vendor.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.online.vendor.dto.ItemResponseDTO;
import com.online.vendor.dto.VendorResponseDTO;
import com.online.vendor.entity.Item;
import com.online.vendor.entity.Vendor;
import com.online.vendor.exception.ItemNameNotFoundException;
import com.online.vendor.exception.VendorNameNotFoundException;
import com.online.vendor.repository.ItemRepository;
import com.online.vendor.repository.VendorRepository;

public class CommonServiceImplTest {

	@InjectMocks
	CommonServiceImpl commonServiceImpl;

	@Mock
	VendorRepository vendorRepository;
	@Mock
	ItemRepository itemRepository;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetVendorsByVendorName() {

		ItemResponseDTO itemResponseDTO = new ItemResponseDTO();
		itemResponseDTO.setItemId(101);
		itemResponseDTO.setItemName("Chicken kabab");
		itemResponseDTO.setPrice(150);
		ItemResponseDTO itemResponseDTO2 = new ItemResponseDTO();
		itemResponseDTO2.setItemId(102);
		itemResponseDTO2.setItemName("Chicken masala");
		itemResponseDTO2.setPrice(180);
		List<ItemResponseDTO> ItemResponseDTOs = new ArrayList<>();
		ItemResponseDTOs.add(itemResponseDTO);
		ItemResponseDTOs.add(itemResponseDTO2);
		String vendorName = "Licious";
		Item item = new Item();
		List<Item> items = new ArrayList<>();
		item.setItemId(101);
		item.setItemName("Chicken kabab");
		item.setItemPrice(150);
		Item item2 = new Item();
		item2.setItemId(102);
		item2.setItemName("Chicken masala");
		item2.setItemPrice(180);
		items.add(item);
		items.add(item2);
		Vendor vendor = new Vendor();
		vendor.setItems(items);
		vendor.setVendorId(1001);

		when(vendorRepository.findByVendorName(vendorName)).thenReturn(vendor);
		List<ItemResponseDTO> responseDTOs = commonServiceImpl.getVendorsByVendorName(vendorName);

		assertEquals(2, responseDTOs.size());

	}

	@Test
	public void testGetVendorsByVendorNameFailure() {
		String vendorName = "Licious";
		when(vendorRepository.findByVendorName(vendorName)).thenReturn(null);
		try {
			commonServiceImpl.getVendorsByVendorName(vendorName);
		} catch (VendorNameNotFoundException e) {
			assertEquals("Vendor name not found", e.getMessage());
		}

	}

	@Test
	public void testGetItemsByItemName() {
		List<VendorResponseDTO> vendorResponseDTOs = new ArrayList<>();
		VendorResponseDTO vendorResponseDTO = new VendorResponseDTO();
		vendorResponseDTO.setItemId(101);
		vendorResponseDTO.setItemName("Chicken Kabab");
		vendorResponseDTO.setPrice(160);
		vendorResponseDTO.setVendorId(1001);
		vendorResponseDTO.setVendorName("Licious");
		vendorResponseDTOs.add(vendorResponseDTO);
		VendorResponseDTO vendorResponseDTO2 = new VendorResponseDTO();
		vendorResponseDTO2.setItemId(101);
		vendorResponseDTO2.setItemName("Chicken Kabab");
		vendorResponseDTO2.setPrice(160);
		vendorResponseDTO2.setVendorId(1002);
		vendorResponseDTO2.setVendorName("Food Panda");
		vendorResponseDTOs.add(vendorResponseDTO);
		vendorResponseDTOs.add(vendorResponseDTO2);

		Vendor vendor = new Vendor();
		List<Vendor> vList = new ArrayList<>();
		vendor.setVendorId(1001);
		vendor.setVendorName("Licious");
		Vendor vendor2 = new Vendor();
		vendor2.setVendorId(1002);
		vendor2.setVendorName("Food Panda");
		vList.add(vendor);
		vList.add(vendor2);
		Item item = new Item();
		item.setItemId(101);
		item.setItemName("Chicken kabab");
		item.setItemPrice(150);
		item.setVendors(vList);

		when(itemRepository.findByItemName("Chicken kabab")).thenReturn(item);
		List<VendorResponseDTO> vendorResponseDTOs2 = commonServiceImpl.getItemsByItemName("Chicken kabab");

		assertEquals(2, vendorResponseDTOs2.size());

	}

	@Test
	public void testGetItemsByItemNameFailure() {

		when(itemRepository.findByItemName("Chicken kabab")).thenReturn(null);
		try {
			commonServiceImpl.getItemsByItemName("Chicken kabab");
		} catch (ItemNameNotFoundException e) {
			assertEquals("Item name not found", e.getMessage());
		}

	}

}
